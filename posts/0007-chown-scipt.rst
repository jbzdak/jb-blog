Set dirs to 755 and files to 644
================================

So I dont forget, if you want to share a directory structure with people
you can change permissions in following way::

    find dir -type d -exec chmod 755 {} \;
    find dir -type f -exec chmod 644 {} \;

In this way others will be able to enter any directory and won't be able to
execuye files.

