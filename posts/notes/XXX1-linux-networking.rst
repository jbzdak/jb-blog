Linux networking


Out of bound communication with VM
----------------------------------

Before messing with routing tables you probably should set up access mechanism
that bypasses networking layer, as **you will break it**.

On Google compute engine you can get read and write access to serial ports
of your virtual machine. ``Documentation for the serial port is here
<https://cloud.google.com/compute/docs/instances/interacting-with-serial-console>``__.

Here is quick recap:

* Enable serial console by issuing: ``gcloud compute instances add-metadata [INSTANCE_NAME] --metadata=serial-port-enable=1``;
* Then connect: ``gcloud compute connect-to-serial-port [INSTANCE_NAME]``;
* If you are asked for the password, you'll need to connect via SSH and set up
  password for your user manually.

Part 1: Use simple routing
--------------------------

You need two instances ``instance-c`` and ``instance-r``. First one will be
a client, and second one will be a router.

Connect to both ``c`` and ``r``. Connect to ``c`` using serial console.

Enable routing in ``r`` --- by default linux hosts are not performing routing,
by: ``sudo sysctl -w net.ipv4.ip_forward=1``.


.. note::

    I guess that enabling routing on public IP is not smart thing to do,
    we'll disable it for any address but IP of ``instance-c`` and probably
    we'll be good.

    Also whole configuration we do here is transient.



