Connecting remote display (projector) to ``i3``
===============================================

It turns out to be suprisingly easy ``randr`` and ``i3`` are integrated well
enough.

I have decided that I'll add a workspace dedicated for projector.


``i3.config`` snippet::

  # switch to workspace
  bindsym $mod+p workspace 11

  bindsym $mod+Shift+p move container to workspace 11

  workspace 11 output VGA-1

To enable display issue::

  xrandr --output VGA-1 --auto --right-of LVDS-1

To disable::

  xrandr --output VGA-1 --off