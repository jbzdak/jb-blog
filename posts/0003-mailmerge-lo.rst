How to use Libre Office serial document (mail merge) functionality
==================================================================

Libre Officess mailmerge functionality is a nice feature that allows
you to create *any kind of serial document*, while all documentation
seems to indicate that it is only usable for creating e-mails.

I used `this tutorial
<http://davidmburke.com/2011/08/10/mail-merge-in-libreoffice/>`_
(it has nice screenshots and so on).

So anyways steps are as follows:

Create data source
------------------

Create a data source, this can be:

1. A Libre Office Base database (or any database)
2. A csv file (**remember to have header row**).
3. An Libre Office Spreadsheet file (**also add a header row**)

Create a document template
--------------------------

Write the text

Attach database to the document
-------------------------------

In writer Edit -> Exchange Database -> Browse -> Select your database file.

In writer View -> Datasources, then select your database file again.

You should see table contents.

Add fields to the document
--------------------------

Now you should be able to Drag and Drop fields (columns) from the table
to the document.

Create serialized documents
---------------------------

Mail Merge Wizard or click the envelope icon in data sources.
This stuff is mostly stupid and deals with preformatted address blocks, etc.
I ve never used it.

1. Select starting Document: Select current document.
2. Select document type: Select letter
3. Insert address block: Uncheck all checkboxes.
4. Create salutation: Uncheck all checkboxes.
