Attention: This blog has moved
==============================

I have moved this blog `to a new home <http://blog.askesis.pl/category/developement.html>`__.

Most of valuable content was moved there, so bookmark new site.
