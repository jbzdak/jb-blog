How to discard connection in a fast way
=======================================

Just a quick note, here is how to drop a TCP connection fast:

* Client sends ``SYN`` packet
* Server responds with ``RST`` packet, which promptly kills the connection
  without any state.